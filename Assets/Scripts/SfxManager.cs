﻿using UnityEngine;
using System.Collections;

public class SfxManager : MonoBehaviour {

    public AudioClip[] explosionClip;
    public AudioClip[] mutationClip;
    public AudioClip[] collectionClip;
    public AudioClip levelCompleteClip;

    AudioSource audioSource;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}
	
    public void PlayExplosion() {

        AudioClip ac = explosionClip[Random.Range( 0, explosionClip.Length )];

        audioSource.clip = ac;
        audioSource.Play();
    }

    public void PlayCollection() {

        AudioClip ac = collectionClip[Random.Range( 0, collectionClip.Length )];

        audioSource.clip = ac;
        audioSource.Play();
    }

    public void PlayMutation() {

        AudioClip ac = mutationClip[Random.Range( 0, mutationClip.Length )];

        audioSource.clip = ac;
        audioSource.Play();
    }

    public void PlayLevelComplete() {

        audioSource.clip = levelCompleteClip;
        audioSource.Play();
    }


}
