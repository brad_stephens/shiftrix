﻿using UnityEngine;
using System.Collections;

public class ShapeletController : MonoBehaviour {

    Shape myShape;
    ShapeController shapeController;
    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    SpawnManager spawnMgr;

    public float fallSpeed;

    bool isInitialized = false;

    bool isShifting = false;
    float shiftPercentage;
    public float shiftRate;
    ShapeType nextShape;

    public ShapeType Type { get { return myShape.type; } }
 
    // Use this for initialization
    void Start() {

        // This isn't called as part of the object pooling !!!
        //Debug.Log( "STARTING!!!" + gameObject.name);

        //shapeController = FindObjectOfType<ShapeController>();
        //myShape = shapeController.NewShape( ShapeType.Square );
        //meshFilter = GetComponent<MeshFilter>();
        //meshRenderer = GetComponent<MeshRenderer>();

        //isInitialized = true;

        //Debug.Log( "Set as initialized in Start for game object "  + gameObject.name);

        //Debug.Log( "Start - Shape Controller: " + shapeController );

    }

    public void Initialize( ShapeType shapeType ) {

        //Debug.Log( "Initialize - Shape Controller: " + shapeController );

        if ( isInitialized == false ) {
            //Debug.Log( gameObject.name + "Shouldn't be here!????" );
            shapeController = FindObjectOfType<ShapeController>();
            spawnMgr = FindObjectOfType<SpawnManager>();

            myShape = shapeController.NewShape( shapeType );
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.material.color = myShape.color;
            isInitialized = true;
            //Debug.Log( "Set as initialized in Initialize for game object " + gameObject.name );

        }

        //Debug.Log( "myShape:" + myShape );

        shapeController.SetShape( myShape, shapeType );
        Mesh mesh = myShape.BuildMesh();
        meshFilter.mesh = mesh;
        meshRenderer.material.color = myShape.color;

    }

    public void Mutate() {
        if ( !isShifting ) {
            isShifting = true;
            nextShape = spawnMgr.GetRandomShape();
            shapeController.MutateShapeStart( myShape, nextShape );
            shiftPercentage = 0.0f;
        }
    }

    // Update is called once per frame
    void Update() {

        transform.Translate( new Vector3( 0, ( fallSpeed * Time.deltaTime * -1 ), 0 ) );

        //if ( Input.GetButtonDown( "Fire1" ) && isShifting == false ) {
        //    isShifting = true;
        //    nextShape = spawnMgr.GetRandomShape();
        //    shapeController.MutateShapeStart( myShape, nextShape );
        //    shiftPercentage = 0.0f;
        //    //Debug.Log( "Shifting " + gameObject.name + " from " + myShape.type + " to " + nextShape );
        //}

        if ( isShifting ) {
            shiftPercentage += Time.deltaTime / shiftRate;
            if ( shiftPercentage < 0.9f ) {
                shapeController.MutateShape( myShape, nextShape, shiftPercentage );
            } else {
                //Debug.Log( "Shift Complete!" );
                shapeController.MutateShape( myShape, nextShape, 1.0f );
                shapeController.MutateShapeFinalize( myShape );
                isShifting = false;
            }

            Mesh mesh = myShape.BuildMesh();
            meshFilter.mesh = mesh;
            meshRenderer.material.color = myShape.color;
        }



    }

    //public void OnCollisionEnter2D( Collision2D collision ) {
    //    Debug.Log( "Shapelet Collision" );
    //}

    //public void OnTriggerEnter2D( Collider2D collision ) {
    //    Debug.Log( "Shapelet Trigger" );

    //}
}
