﻿using UnityEngine;
using System.Collections;

public class Shape {

    public Vector3[] vertices;
    Vector3[] morphStartverticies;
    public int[] triangles;
    bool isMutating = false;
    public ShapeType type;
    public ShapeType ToType;
    public Color color;

    public Shape() {
        vertices = new Vector3[8];
        morphStartverticies = new Vector3[8];
        triangles = new int[6 * 3];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;

        triangles[3] = 0;
        triangles[4] = 2;
        triangles[5] = 3;

        triangles[6] = 0;
        triangles[7] = 3;
        triangles[8] = 4;

        triangles[9] = 0;
        triangles[10] = 4;
        triangles[11] = 5;

        triangles[12] = 0;
        triangles[13] = 5;
        triangles[14] = 6;

        triangles[15] = 0;
        triangles[16] = 6;
        triangles[17] = 7;

    }

    public Shape(Shape protoShape) {
        vertices = new Vector3[8];
        morphStartverticies = new Vector3[8];

        for(int i=0; i<vertices.Length; i++ ) {
            this.vertices[i] = protoShape.vertices[i];
        }

        type = protoShape.type;
        color = protoShape.color;
        //Debug.Log( "Setting Color to " + color );

        //this.vertices = protoShape.vertices;
        this.triangles = protoShape.triangles;
    }

    public Mesh BuildMesh() {

        Mesh mesh = new Mesh();

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

        return mesh;
    }

    public void Mutate(Shape toShape, float amount) {

        //if(isMutating == false ) {
        //    // record my previous vertices
        //    for(int i=0; i<vertices.Length; i++ ) {
        //        morphStartverticies[i] = vertices[i];
        //    }
        //    isMutating = true;
        //}

        //Debug.Log( "Mutation: " + vertices[1] + " to " + toShape.vertices[1] );

        for(int i=0; i<vertices.Length; i++ ) {
            vertices[i] = Vector3.Lerp( morphStartverticies[i], toShape.vertices[i], amount );
        }

        // Mutate Color
        color = Color.Lerp( color, toShape.color, amount );

        //if(amount >= 0.99f) {
        //    isMutating = false;
        //}
    }

    public void StartMutation() {
        // record my previous vertices
        for ( int i = 0; i < vertices.Length; i++ ) {
            morphStartverticies[i] = vertices[i];
        }
    }

    public void FinalizeMutation() {
        type = ToType;
    }

}
