﻿using UnityEngine;
using System.Collections.Generic;

public enum ShapeType { Square, Triangle, Octagon, Diamond, Rectangle, Trapazoid, Hexagon };

public class ShapeController : MonoBehaviour {

    Dictionary<ShapeType, Shape> shapePrototypes;
    Shape shape;
    public Color[] shapeColors;

    void Awake() {
        // Create shape prototypes
        CreateShapePrototypes();
    }

    //public ShapeController() {

    //    // Create shape prototypes
    //    CreateShapePrototypes();
    //}

    void CreateShapePrototypes() {

        shapePrototypes = new Dictionary<ShapeType, Shape>();

        // Square
        shape = new Shape();
        // Upper Left
        shape.vertices[0] = new Vector3( -0.5f, 0.5f, 0 );
        shape.vertices[1] = new Vector3( -0.5f, 0.5f, 0 );

        // Upper right
        shape.vertices[2] = new Vector3( 0.5f, 0.5f, 0 );
        shape.vertices[3] = new Vector3( 0.5f, 0.5f, 0 );

        // Lower right
        shape.vertices[4] = new Vector3( 0.5f, -0.5f, 0 );
        shape.vertices[5] = new Vector3( 0.5f, -0.5f, 0 );

        // Lower Left
        shape.vertices[6] = new Vector3( -0.5f, -0.5f, 0 );
        shape.vertices[7] = new Vector3( -0.5f, -0.5f, 0 );

        shape.type = ShapeType.Square;
        shape.color = shapeColors[(int)ShapeType.Square];
        shapePrototypes.Add( ShapeType.Square, shape );

        // Triangle
        shape = new Shape();
        shape.vertices[0] = new Vector3( -0.5f, -0.5f, 0 );
        shape.vertices[1] = new Vector3(  0.0f, 0.5f, 0 );
        shape.vertices[2] = new Vector3( 0.0f, 0.5f, 0 );
        shape.vertices[3] = new Vector3( 0.5f, -0.5f, 0 );
        shape.vertices[4] = new Vector3( 0.5f, -0.5f, 0 );
        shape.vertices[5] = new Vector3( 0.5f, -0.5f, 0 );
        shape.vertices[6] = new Vector3( -0.5f, -0.5f, 0 );
        shape.vertices[7] = new Vector3( -0.5f, -0.5f, 0 );

        shape.type = ShapeType.Triangle;
        shape.color = shapeColors[(int)ShapeType.Triangle];
        shapePrototypes.Add( ShapeType.Triangle, shape );

        // Octagon
        shape = new Shape();
        shape.vertices[0] = new Vector3( -0.5f, 0.17f, 0 );
        shape.vertices[1] = new Vector3( -0.17f, 0.5f, 0 );
        shape.vertices[2] = new Vector3( 0.17f, 0.5f, 0 );
        shape.vertices[3] = new Vector3( 0.5f, 0.17f, 0 );
        shape.vertices[4] = new Vector3( 0.5f, -0.17f, 0 );
        shape.vertices[5] = new Vector3( 0.17f, -0.5f, 0 );
        shape.vertices[6] = new Vector3( -0.17f, -0.5f, 0 );
        shape.vertices[7] = new Vector3( -0.5f, -0.17f, 0 );

        shape.type = ShapeType.Octagon;
        shape.color = shapeColors[(int)ShapeType.Octagon];
        shapePrototypes.Add( ShapeType.Octagon, shape );

        // Diamond
        shape = new Shape();
        shape.vertices[0] = new Vector3( -0.5f, 0.0f, 0 );
        shape.vertices[1] = new Vector3( 0.0f, 0.5f, 0 );
        shape.vertices[2] = new Vector3( 0.0f, 0.5f, 0 );
        shape.vertices[3] = new Vector3( 0.5f, 0.0f, 0 );
        shape.vertices[4] = new Vector3( 0.5f, 0.0f, 0 );
        shape.vertices[5] = new Vector3( 0.0f, -0.5f, 0 );
        shape.vertices[6] = new Vector3( 0.0f, -0.5f, 0 );
        shape.vertices[7] = new Vector3( -0.5f, 0.0f, 0 );

        shape.type = ShapeType.Diamond;
        shape.color = shapeColors[(int)ShapeType.Diamond];
        shapePrototypes.Add( ShapeType.Diamond, shape );

        // Rectangle
        shape = new Shape();
        shape.vertices[0] = new Vector3( -0.5f, 0.17f, 0 );
        shape.vertices[1] = new Vector3( -0.5f, 0.17f, 0 );
        shape.vertices[2] = new Vector3( 0.5f, 0.17f, 0 );
        shape.vertices[3] = new Vector3( 0.5f, 0.17f, 0 );
        shape.vertices[4] = new Vector3( 0.5f, -0.17f, 0 );
        shape.vertices[5] = new Vector3( 0.5f, -0.17f, 0 );
        shape.vertices[6] = new Vector3( -0.5f, -0.17f, 0 );
        shape.vertices[7] = new Vector3( -0.5f, -0.17f, 0 );

        shape.type = ShapeType.Rectangle;
        shape.color = shapeColors[(int)ShapeType.Rectangle];
        shapePrototypes.Add( ShapeType.Rectangle, shape );

        // Trapazoid
        shape = new Shape();
        shape.vertices[0] = new Vector3( -0.2f, 0.2f, 0 );
        shape.vertices[1] = new Vector3( -0.2f, 0.2f, 0 );
        shape.vertices[2] = new Vector3( 0.2f, 0.2f, 0 );
        shape.vertices[3] = new Vector3( 0.2f, 0.2f, 0 );
        shape.vertices[4] = new Vector3( 0.5f, -0.2f, 0 );
        shape.vertices[5] = new Vector3( 0.5f, -0.2f, 0 );
        shape.vertices[6] = new Vector3( -0.5f, -0.2f, 0 );
        shape.vertices[7] = new Vector3( -0.5f, -0.2f, 0 );

        shape.type = ShapeType.Trapazoid;
        shape.color = shapeColors[(int)ShapeType.Trapazoid];
        shapePrototypes.Add( ShapeType.Trapazoid, shape );

        // Hexagon
        shape = new Shape();
        shape.vertices[0] = new Vector3( -0.5f, 0.2f, 0 );
        shape.vertices[1] = new Vector3( 0.0f, 0.5f, 0 );
        shape.vertices[2] = new Vector3( 0.0f, 0.5f, 0 );
        shape.vertices[3] = new Vector3( 0.5f, 0.2f, 0 );
        shape.vertices[4] = new Vector3( 0.5f, -0.2f, 0 );
        shape.vertices[5] = new Vector3( 0.0f, -0.5f, 0 );
        shape.vertices[6] = new Vector3( 0.0f, -0.5f, 0 );
        shape.vertices[7] = new Vector3( -0.5f, -0.2f, 0 );

        shape.type = ShapeType.Hexagon;
        shape.color = shapeColors[(int)ShapeType.Hexagon];
        shapePrototypes.Add( ShapeType.Hexagon, shape );


        Debug.Log( "Prototype shapes created" );
    }

    public Shape NewShape(ShapeType prototype) {

        if(shapePrototypes.ContainsKey(prototype)) {

            //Debug.Log( "Creating new shape" );
            Shape shape = new Shape( shapePrototypes[prototype] );
            shape.type = prototype;
            return shape;
        }

        Debug.LogError( "NewShape - We don't ahve prototype for shape type " + prototype );
        return null;

    }

    public void MutateShapeStart( Shape shape, ShapeType type ) {
        shape.StartMutation();
        shape.ToType = type;
    }

    public void MutateShapeFinalize( Shape shape) {
        shape.FinalizeMutation();
    }

    public void MutateShape(Shape shape, ShapeType type, float amount) {
        shape.Mutate( shapePrototypes[type], amount );
    }

    public void SetShape( Shape shape, ShapeType prototype ) {

        if ( shapePrototypes.ContainsKey( prototype ) ) {

            Shape newShape = shapePrototypes[prototype];

            for ( int i = 0; i < shape.vertices.Length; i++ ) {
                shape.vertices[i] = newShape.vertices[i];
            }

            shape.type = prototype;
            shape.color = shapePrototypes[prototype].color;
        }
    }


    public void UpdateShape(Shape shape, float deltaTime) {

    }

    public ShapeType GetRandomShape() {
        int i = Random.Range( 0, shapePrototypes.Count );
        return (ShapeType)i;
    }

    public void DisplayData(Shape shape) {
        Debug.Log( shape.triangles.Length + " Triangle points" );
        for ( int i = 0; i < shape.triangles.Length; i += 3 ) {
            Debug.Log( "Triangle " + (i+1) + ": " + shape.triangles[i] + " " + shape.triangles[i+1] + " " + shape.triangles[i + 2] );
        }
        for ( int i = 0; i < shape.vertices.Length; i ++) {
            Debug.Log( "Vert " + i + ": " + shape.vertices[i] );
        }

    }

}
