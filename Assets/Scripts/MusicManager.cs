﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public AudioClip levelMusicClip;
    AudioSource audioSource;

	// Use this for initialization
	void Awake () {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = levelMusicClip;
	}

    public void StartLevelMusic() {
        audioSource.Play();
    }

    public void StopLevelMusic() {
        audioSource.Stop();
    }
}
