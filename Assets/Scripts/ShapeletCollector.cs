﻿using UnityEngine;
using System.Collections;

public class ShapeletCollector : MonoBehaviour {

    SpawnManager spawnController;

    System.Action<string> CollectorCollidedCallback;

    // Use this for initialization
    void Start() {

        spawnController = FindObjectOfType<SpawnManager>();
    }

    public void SetCollectorCollidedCallback(System.Action<string> cb) {
        CollectorCollidedCallback += cb;
    }

    //public void DisableCollection() {
    //    GetComponent<BoxCollider2D>().enabled = false;
    //}

    //public void EnableCollection() {
    //    GetComponent<BoxCollider2D>().enabled = true;
    //}

    public void OnTriggerEnter2D( Collider2D collision ) {
        //Debug.Log( collision.gameObject.name + " hit the collector" );

        if(CollectorCollidedCallback != null ) {
            CollectorCollidedCallback( collision.name );
        }

        //spawnController.Recycle( collision.gameObject );
    }


    // Update is called once per frame
    void Update() {

    }
}
