﻿using UnityEngine;
using System.Collections;


public class Level : MonoBehaviour {

    [SerializeField]
    public ShapeType[] availbleShapes;

    [SerializeField]
    public float shapeletSpawnRate;

    [SerializeField]
    public float shapeletFallSpeed;

}
