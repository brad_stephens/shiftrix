﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    ShapeController shapeController;
    SfxManager sfxMgr;
    SpawnManager spawnMgr;
    ScoreManager scoreMgr;
    PlayerController playerController;
    ShapeletCollector collector;
    MusicManager musicMgr;

    public Text levelCompleteText;
    public Animator levelCompleteAnimator;

    public Text nextLevelText;
    public Animator nextLevelAnimator;

    public Text gameOverText;
    public Animator gameOverAnimator;

    public Image lifeBarImage;
    public RectTransform lifeBarRect;

    public RectTransform instructionPanel;

    public LevelData[] levels;

    bool levelDraining = false;
    bool gameOver = false;
    bool resettingLevel = false;
    bool instructionsShowing = true;

    int currentLevel;
    public int[] levelAvailableObjects;
    public float[] levelSpawnRate;
    public float[] levelFallSpeed;

	// Use this for initialization

    void Awake() {
        //shapeController = new ShapeController();

    }

    
    void Start () {
        sfxMgr = FindObjectOfType<SfxManager>();
        spawnMgr = FindObjectOfType<SpawnManager>();
        scoreMgr = FindObjectOfType<ScoreManager>();
        collector = FindObjectOfType<ShapeletCollector>();
        playerController = FindObjectOfType<PlayerController>();
        musicMgr = FindObjectOfType<MusicManager>();

        // Setup Callbacks
        playerController.SetPlayerCollidedCallback( OnPlayerCollided );
        collector.SetCollectorCollidedCallback( OnCollectorCollided );

        currentLevel = 0;

        SetupLevel(currentLevel);

        //spawnMgr.EnableSpawning();

        //musicMgr.StartLevelMusic();
    }

    //public void PlayerMutate
    // Update is called once per frame
    void Update () {
	
        if(instructionsShowing) {
            if ( Input.anyKey ) {
                instructionPanel.gameObject.SetActive(false);
                instructionsShowing = false;
                spawnMgr.EnableSpawning();
                musicMgr.StartLevelMusic();
            }

            return;
        }

        if(levelDraining == true && !gameOver) {
            
            // We are ending a level, wait until level is clear
            if ( !spawnMgr.LevelIsClear() )
                return;

            levelDraining = false;

            // Level has finished draining, setup next level
            if(resettingLevel) {
                currentLevel = -1;
                resettingLevel = false;
            }

            currentLevel++;

            int runLevel = currentLevel;

            if(currentLevel >= levelAvailableObjects.Length ) {
                runLevel = levelAvailableObjects.Length - 1;
                playerController.moveSpeed += 1;
            }

            nextLevelText.text = "Level " + (currentLevel + 1);
            nextLevelAnimator.SetTrigger( "LevelComplete" );

            SetupLevel( runLevel );
            scoreMgr.ResetScore();
            spawnMgr.EnableSpawning();
            playerController.EnableCollection();
            levelDraining = false;
        }

        if ( gameOver && Input.anyKey ) {

            musicMgr.StopLevelMusic();
            SceneManager.LoadScene( "Title" );
        }

        if ( resettingLevel ) {
            SetupLevel( currentLevel );
            spawnMgr.EnableSpawning();
            playerController.EnableCollection();
        }
    }

    void SetupLevel(int level) {

        SetupAvailablShapes( level );
        spawnMgr.spawnRate = levelSpawnRate[level];
        spawnMgr.fallSpeed = levelFallSpeed[level];

        SetLifeBar( 50 );

        Debug.Log( "Setting level " + level + " to shapes=" + levelAvailableObjects[level] + ", SpawnRate="
            + levelSpawnRate[level] + ", fallSpeed=" + levelFallSpeed[level] );
    }

    void SetupAvailablShapes( int level) {
        spawnMgr.ResetAvailableShapes();
        for(int i=0; i<levelAvailableObjects[level]; i++ ) {
            spawnMgr.AddAvailableShape( (ShapeType)i );
        }
    }

    void SetLifeBar( int value ) {

        int setValue = value;

        if ( value > 400 )
            setValue = 400;
        else if ( value < 0 )
            setValue = 0;

        //Debug.Log( "Setting lifebar value to " + setValue );
        Vector2 lifeBarSize = new Vector2( lifeBarRect.sizeDelta.x, setValue );
        lifeBarRect.sizeDelta = lifeBarSize;
        Color lifeBarColor = Color.green;
        if ( setValue <= 20 )
            lifeBarColor = Color.red;

        lifeBarImage.color = lifeBarColor;
    }

    int GetLifeBarValue() {
        return ( (int)lifeBarRect.sizeDelta.y );
    }

    void OnPlayerCollided(string objName) {
        //Debug.Log( "Player Collided with " + objName );

        // Find game object from Spawner
        GameObject go = spawnMgr.GetSpawnedObjectByName( objName );

        // Check if this is a collection or a mutation
        ShapeletController sc = go.GetComponent<ShapeletController>();
        if(sc.Type == playerController.shapeType) {
            //Debug.Log( "We hit Ourselves!!" );

            spawnMgr.Recycle( go );

            sfxMgr.PlayCollection();

            //spawnMgr.SpawnCollection( go.transform.position, playerController.PlayerPosition );

            scoreMgr.AddPoints( 10  * (currentLevel+1));

            SetLifeBar( GetLifeBarValue() + 20 );


        } else {
            // We hit something else
            //Debug.Log( "We hit something else!!" );

            spawnMgr.Recycle( go );

            sfxMgr.PlayMutation();

            playerController.NextShapeType = sc.Type;

            // Adjust score
            SetLifeBar( Mathf.RoundToInt(GetLifeBarValue() * 0.75f) );

        }

        CheckScore();

    }

    void OnCollectorCollided(string objName) {

        // Find game object from Spawner
        GameObject go = spawnMgr.GetSpawnedObjectByName( objName );

        if ( levelDraining == true ) {
            spawnMgr.Recycle( go );
            return;
        }

        // Check if this is a collection or a mutation
        ShapeletController sc = go.GetComponent<ShapeletController>();

        if ( sc.Type == playerController.shapeType ) {
            // We let our shape get away

            //Debug.Log( "We let one go!!" );

            // Recycle Shapelet
            spawnMgr.Recycle( go );

            // Play collection sound
            sfxMgr.PlayExplosion();

            // Play particles
            spawnMgr.SpawnExplosion( go.transform.position );

            // Adjust score
            scoreMgr.AddPoints( -10 * ( currentLevel + 1 ) );
            SetLifeBar( GetLifeBarValue() - 25 );

            // Mutate ourselves
            playerController.NextShapeType = spawnMgr.GetRandomShape();

            // Mutate all shapes
            spawnMgr.MutateAllObjects();

        } else {
            // We hit something else
            //Debug.Log( "We hit something else!!" );

            // Recycle Shapelet
            spawnMgr.Recycle( go );
        }

        CheckScore();

    }

    public struct LevelData {
        public int availableShapes;
        public float shapeletSpawnRate;
        public float shapeletFallSpeed;

        public LevelData(int shapes, float spawnRate, float fallSpeed) {
            availableShapes = shapes;
            shapeletSpawnRate = spawnRate;
            shapeletFallSpeed = fallSpeed;
        }
    }

    void CheckScore() {
        if(GetLifeBarValue() >= 400 ) {
            levelDraining = true;
            Debug.Log( "Level Complete!" );
            spawnMgr.DisableSpawning();
            playerController.DisableCollection();

            //levelCompleteText.gameObject.SetActive( true );
            levelCompleteText.enabled = true;
            levelCompleteAnimator.SetTrigger( "LevelComplete" );
            sfxMgr.PlayLevelComplete();
        } else if( GetLifeBarValue() <= 0 ) {
            Debug.Log( "Game Over!" );
            levelDraining = true;
            spawnMgr.DisableSpawning();
            playerController.DisableCollection();
            gameOverText.enabled = true;
            gameOverAnimator.SetTrigger( "TriggerText" );
            playerController.Die();
            sfxMgr.PlayExplosion();
            gameOver = true;
        }

    }
}
