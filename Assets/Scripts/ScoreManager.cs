﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text scoreText;
    public int currentScore = 0;

    public int Score {
        get {
            return currentScore;
        }
    }
                                       // Use this for initialization
    void Start () {

        ResetScore();
	}
	
    public void ResetScore() {
        scoreText.text = "Score: 0";
        currentScore = 0;
    }

    public void AddPoints(int amount) {
        currentScore += amount;

        if ( currentScore < 0 )
            currentScore = 0;

        scoreText.text = "Score: " + currentScore;
    }

}
