﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public float horizontalMax;
    public float verticalMax;
    public float verticalMin;

    public float morphRate;
    float morphAmount;
    
    ShapeType myShapeType = ShapeType.Triangle;
    ShapeType myNextShapeType;

    public ShapeType NextShapeType {
        set { myNextShapeType = value;
              morphAmount = 0.0f;
              shapeController.MutateShapeStart( myShape, value );
        }
    }

    public ShapeType shapeType { get { return myShapeType; } }
    public Vector3 PlayerPosition { get { return transform.position; } }

    public ParticleSystem explosionParticles;

    Shape myShape;
    System.Action<string> PlayerCollidedCallback; 

    ScoreManager scoreManager;
    ShapeController shapeController;
    SpawnManager spawnMgr;
    MeshFilter meshFilter;
    MeshRenderer meshRenderer;

    // Use this for initialization
    void Start() {

        scoreManager = FindObjectOfType<ScoreManager>();
        shapeController = FindObjectOfType<ShapeController>();
        spawnMgr = FindObjectOfType<SpawnManager>();
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        // Set our initial shape.
        myShape = shapeController.NewShape( myShapeType );

        meshFilter.mesh = myShape.BuildMesh();
         
    }

    public void SetPlayerCollidedCallback(System.Action<string> cb) {

        //Debug.Log( "Setting Callback!!!" );
        PlayerCollidedCallback += cb;
    }

    // Update is called once per frame
    void Update() {

        MovePlayer();

        if(myShapeType != myNextShapeType) {
            Morph();
        }

        // Used only for testing
        if ( Input.GetButtonDown( "Fire1" ) ) {
            spawnMgr.MutateAllObjects();
        }


    }

    public void DisableCollection() {
        GetComponent<CircleCollider2D>().enabled = false;
    }

    public void EnableCollection() {
        GetComponent<CircleCollider2D>().enabled = true;
    }

    public void Die() {
        spawnMgr.SpawnExplosion( transform.position );
        GetComponent<MeshRenderer>().enabled = false;
    }

    void MovePlayer() {

        float deltaX = Input.GetAxis( "Horizontal" ) * moveSpeed * Time.deltaTime;
        float deltaY = Input.GetAxis( "Vertical" ) * moveSpeed * Time.deltaTime;

        //Debug.Log( "DeltaX: " + deltaX + ", DeltaY:" + deltaY );

        float newX = transform.position.x + deltaX;
        float newY = transform.position.y + deltaY;

        newX = Mathf.Clamp( newX, -horizontalMax, horizontalMax );
        newY = Mathf.Clamp( newY, verticalMin, verticalMax );

        //Debug.Log( "newX: " + newX + ", newY:" + newY );

        transform.position = new Vector3( newX, newY, 0 );

    }

    void Morph() {

        morphAmount += Time.deltaTime / morphRate;

        shapeController.MutateShape(myShape, myNextShapeType, morphAmount );
        meshFilter.mesh = myShape.BuildMesh();
        meshRenderer.material.color = myShape.color;

        if(morphAmount >= 1.0f) {
            myShapeType = myNextShapeType;
            shapeController.MutateShapeFinalize( myShape );
        }
    }


    public void OnTriggerEnter2D( Collider2D collision ) {
        //Debug.Log( "I was Triggered! " +collision.tag );

        if(collision.tag == "Shapelet" ) {

            //Debug.Log( "Hit Shapelet " + collision.name );

            if(PlayerCollidedCallback != null) {

               // Debug.Log( "Calling PlayerCollided callback" );
                PlayerCollidedCallback( collision.name );
            }

            //ShapeletController sc = collision.GetComponent<ShapeletController>();

            //if(sc.Type == myShapeType) {
            //    //Debug.Log( " YAY! I got points " );

            //    spawnController.Destroy( collision.gameObject );
            //    scoreManager.AddPoints( 10 );
            //} else {
            //    // I hit something else, time to morph
            //    scoreManager.AddPoints( -10 );

            //    spawnController.Recycle( collision.gameObject );
                
            //    myNextShapeType = sc.Type;

            //    shapeController.MutateShapeStart( myShape, myNextShapeType );
            //    morphAmount = 0.0f;

            //}
        }
    }
}
