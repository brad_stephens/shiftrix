﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour {

    ShapeController sc;
    SfxManager sfxManager;

    List<ShapeType> availableShapes;
    Dictionary<string, GameObject> spawnedObjectMap;

    public GameObject spawnablePrefab;

    Queue<GameObject> spawnPool;
    // The size of the pool of spawnable objects
    public int spawnPoolSize;

    Queue<GameObject> explosionParticlesPool;
    public int particlePoolSize;

    Queue<GameObject> collectionParticlesPool;

    // ToDo: Make this private at some point
    public float spawnRate;
    public float fallSpeed;

    float spawnTimer;

    public GameObject explosionParticlesPrefab;
    public GameObject collectionParticlesPrefab;

    // Spawn constraints
    float spawnY;
    float spawnXMin;
    float spawnXMax;

    bool spawningEnabled;

    void Awake() {
        availableShapes = new List<ShapeType>();

        Debug.Log( "availableShapes is initialized" );

    }

    // Use this for initialization
    void Start () {

        sc = FindObjectOfType<ShapeController>();
        sfxManager = FindObjectOfType<SfxManager>();

        spawnedObjectMap = new Dictionary<string, GameObject>();
        CalculateSpawnConstraints();

        SetupSpawnPool( spawnPoolSize );
        SetupParticlePool( particlePoolSize );

        spawnTimer = spawnRate;
    
    }

    // Update is called once per frame
    void Update() {

        if ( spawningEnabled ) {
            spawnTimer -= Time.deltaTime;

            if ( spawnTimer <= 0 ) {

                SpawnObject();
                spawnTimer = spawnRate;
            }
        }
    }

    public void AddAvailableShape(ShapeType type) {
        //Debug.Log( "Adding shape " + type );
        availableShapes.Add( type );
    }

    public void ResetAvailableShapes() {
        // Remove existing available shapes
        availableShapes.Clear();
    }

    void CalculateSpawnConstraints() {

        Transform spawnArea = transform.GetChild( 0 ).transform;

        spawnY = spawnArea.position.y;
        spawnXMin = spawnArea.position.x - spawnArea.localScale.x;
        spawnXMax = spawnArea.position.x + spawnArea.localScale.x;
        //Debug.Log( "Spawn Area: Y=" + spawnY + ", XMin=" + spawnXMin + ", XMax=" + spawnXMax );
    }

    void SetupSpawnPool(int size) {
        // Setup a pool of spawnable objects

        spawnPool = new Queue<GameObject>();

        for(int i=0; i< size;  i++) {
            GameObject go = (GameObject)Instantiate( spawnablePrefab, Vector3.zero, Quaternion.identity );
            go.name = "Spawnable_" + i;
            go.SetActive( false );
            go.transform.SetParent( this.transform );

            spawnPool.Enqueue( go );

            //spawnedObjectMap.Add( go.name, go );
        }
    }

    void SetupParticlePool( int size ) {
        // Setup a pool of Particle Game objects

        explosionParticlesPool = new Queue<GameObject>();

        for ( int i = 0; i < size; i++ ) {
            GameObject go = (GameObject)Instantiate( explosionParticlesPrefab, Vector3.zero, explosionParticlesPrefab.transform.rotation);
            go.name = "Explosion_" + i;
            go.transform.localScale = Vector3.one;
            go.transform.SetParent( this.transform );

            explosionParticlesPool.Enqueue( go );
        }

        collectionParticlesPool = new Queue<GameObject>();

        for ( int i = 0; i < size; i++ ) {
            GameObject go = (GameObject)Instantiate( collectionParticlesPrefab, Vector3.zero, collectionParticlesPrefab.transform.rotation );
            go.name = "Collection_" + i;
            go.transform.localScale = Vector3.one;
            go.transform.SetParent( this.transform );

            collectionParticlesPool.Enqueue( go );
        }

        Debug.Log( "Setup " + particlePoolSize + " explosion game objects" );
    }

    public void SpawnExplosion( Vector3 position ) {

        if ( explosionParticlesPool.Count == 0 ) {
            Debug.LogError( "Out of Explosion Particle objects" );
            return;
        }

        GameObject go = explosionParticlesPool.Dequeue();
        go.transform.position = position;
        go.GetComponent<ParticleSystem>().Play();
        //sfxManager.PlayExplosion();
        explosionParticlesPool.Enqueue( go );
    }

    public void SpawnCollection( Vector3 position, Vector3 target) {

        if ( collectionParticlesPool.Count == 0 ) {
            Debug.LogError( "Out of Collection Particle objects" );
            return;
        }

        GameObject go = collectionParticlesPool.Dequeue();
        go.transform.position = position;
        go.transform.LookAt( target );
        go.GetComponent<ParticleSystem>().Play();
        //sfxManager.PlayExplosion();
        collectionParticlesPool.Enqueue( go );
    }


    void SpawnObject() {

        if(spawnPool.Count == 0) {
            Debug.LogError( "Out of spawnable objects in the pool" );
            return;
        }

        //Debug.Log( "Spawning Object" );
        Vector3 spawnPosition = GetRandomSpawnPosition();
        GameObject go = spawnPool.Dequeue();

        go.transform.position = spawnPosition;

        go.SetActive( true );
        go.GetComponent<ShapeletController>().Initialize( GetRandomShape() );
        go.GetComponent<ShapeletController>().fallSpeed = fallSpeed;
        spawnedObjectMap.Add( go.name, go );

        //MeshRenderer mr = go.GetComponent<MeshRenderer>();


    }

    public bool LevelIsClear() {
        return ( spawnPool.Count == spawnPoolSize );
    }

    public ShapeType GetRandomShape() {

        if(availableShapes.Count == 0 ) {
            Debug.LogError( "GetRandomShape - No Available Shapes" );
            return ShapeType.Square;
        }

        return (ShapeType)Random.Range(0, availableShapes.Count);
    }

    public GameObject GetSpawnedObjectByName(string objName) {
        if( spawnedObjectMap.ContainsKey( objName ) ) {
            return spawnedObjectMap[objName];
        }
        else {
            Debug.LogError( "GetSpawnedObjectByName - do not have spawned object with name" + objName );
            return null;
        }
    }

    //public void Destroy( GameObject go ) {
    //    // Used to destroy a shapelet and show effects/play sounds
    //    SpawnExplosion( go.transform.position );
    //    Recycle( go );
    //}

    public void MutateAllObjects() {
        // tell all active shapelets to mutate
        foreach(string name in spawnedObjectMap.Keys) {
            spawnedObjectMap[name].GetComponent<ShapeletController>().Mutate();
        }
    }

    public void Recycle( GameObject go ) {

        //Debug.Log( "Recycling " + go.name );

        go.SetActive( false );
        spawnedObjectMap.Remove( go.name);
        spawnPool.Enqueue( go );
    }

    Vector3 GetRandomSpawnPosition() {

        Vector3 position = new Vector3(Random.Range(spawnXMin, spawnXMax ), spawnY, 0);
        //Debug.Log( "Spawn Position = " + position );
        return position;
    }

    public void DisableSpawning() {
        spawningEnabled = false;
    }
    public void EnableSpawning() {
        spawningEnabled = true;
    }
}
