﻿using UnityEngine;
using System.Collections;

public class ShapeCollision : MonoBehaviour {

    public void OnTriggerEnter2D( Collider2D collision ) {
        Debug.Log( collision.gameObject.name + " has collided with me" );

    }

    public void OnTriggerExit2D( Collider2D collision ) {
        Debug.Log( collision.gameObject.name + " is no longer colliding with me" );

    }

}
